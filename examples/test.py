from os import path
from monsurvey.core.plot import Plotter
from monsurvey.core.requirement import Requirement, Subformula

from monsurvey.monitors.tltk import TLTkSpecification
from monsurvey.monitors.rtamt import DiscreteSpecification, DenseSpecification
from monsurvey.monitors import pytaliro
from monsurvey.monitors import staliro

import numpy as np
import tltk_mtl as mtl

if __name__ == "__main__":
    traces = [1, 2, 3, 2, 1]
    times = [0.0, 0.1, 0.2, 0.3, 0.4]

    tltk = TLTkSpecification(
        Requirement([
            Subformula("phi", mtl.Finally(0, 0.2, mtl.Predicate("b", -1.0, -1.0)))
        ]),
        {"b": 0}
    )

    rtamt_discrete = DiscreteSpecification(
        Requirement([
            Subformula("phi", "eventually[0, 0.2] (b >= 1.0)")
        ]),
        {"b": 0}
    )

    rtamt_dense = DenseSpecification(
        Requirement([
            Subformula("phi", "eventually[0, 0.2] (b >= 1.0)")
        ]),
        {"b": 0}
    )

    pytp_taliro = pytaliro.TpTaliroSpecification(
        Requirement([
            Subformula("phi", "@Var_t <>(({ Var_t >= 0 } /\ { Var_t <= 0.2 }) /\ (b))")
        ]),
        [{"name": "b", "a": -1.0, "b": -1.0}]
    )

    dp_taliro = staliro.DpTaliroSpecification(
        path.join(path.expanduser("~"), "dp_taliro"), # set to path to MATLAB DP-TaLiRo
        Requirement([
            Subformula("phi", "<>_[0, 0.2] b")
        ]),
        [{
            "str": "b",
            "A": -1.0,
            "b": -1.0
        }]
    )

    tp_taliro = staliro.TpTaliroSpecification(
        path.join(path.expanduser("~"), "tp_taliro"), # set to path to MATLAB TP-TaLiRo
        Requirement([
            Subformula("phi", "@Var_t <>(({ Var_t >= 0 } /\ { Var_t <= 0.2 }) /\ (b))")
        ]),
        [{
            "str": "b",
            "A": -1.0,
            "b": -1.0
        }]
    )

    results = [
        tltk.evaluate(traces, times),
        rtamt_discrete.evaluate(traces, times),
        rtamt_dense.evaluate(traces, times),
        pytp_taliro.evaluate(traces, times),
        dp_taliro.evaluate(traces, times),
        tp_taliro.evaluate(traces, times)
    ]

    plot = Plotter(results)
    plot.robustness_per_timestamp(save=True, filename="result.svg", title="Robustness per Timestamp (F[0, 0.2] b >= 1.0)")

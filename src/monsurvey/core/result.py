from dataclasses import dataclass
from typing import Dict, List

@dataclass
class Evaluation:
    """Resulting monitor robustness evaluation metric.
    """

    elapsed: float
    robustness: float
    timestamp: float

class Result:
    """Robustness evaluation for each subformula at each timestep.
    """

    _label: str
    _evaluations: Dict[str, List[Evaluation]]

    def __init__(self, label: str=""):
        self._label = label
        self._evaluations = {}

    def append(self, key, robustness):
        """Append a robustness to a specified subformula.
        """

        if key not in self._evaluations:
            self._evaluations[key] = []

        self._evaluations[key].append(robustness)

    @property
    def label(self) -> str:
        return self._label

    @property
    def evaluations(self) -> Dict[str, List[Evaluation]]:
        return self._evaluations

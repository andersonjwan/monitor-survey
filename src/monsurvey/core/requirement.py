from __future__ import annotations

from dataclasses import dataclass
from typing import Any, List

@dataclass
class Subformula:
    """Undividable component of a requirement.

    Attributes:
        label: A label for the subformula to help distinguish in evaluation.
        phi: The representation of a subformula for the tool.
    """

    label: str
    phi: Any

class Requirement:
    """Divided representation of a requirement.

    A requirement is constructed of multiple sub-formulas to create one complete
    sub-formula (e.g., a /\ <> b consists of: (i) a, (ii) <> b).

    Attributes:
        requirement: A vector of Subformulas.
    """

    _requirement: List[Subformula]

    def __init__(self, requirement: List[Subformula]=[]) -> None:
        self._requirement = requirement

    def __iter__(self):
        return iter(self._requirement)

    def put(self, subformula: Subformula) -> Requirement:
        """Append a subformula to the requirement.
        """

        self._requirement.append(subformula)
        return self

    def __str__(self) -> str:
        return str(self._requirement)

from abc import ABC, abstractmethod
from typing import Any

import numpy as np
import numpy.typing as npt

from .requirement import Requirement

class Specification(ABC):
    """Interface for monitor specifications.
    """

    @abstractmethod
    def evaluate(self, requirement: Requirement, traces: npt.ArrayLike, timestamps: npt.ArrayLike) -> float:
        """Evaluate the robustness of a complete requirement.

        Arguments:
            traces: Set of trajectories.
            timestamps: List of corresponding timestamps.

        Returns:
            The robustness metric of the requirement over the entire trace.
        """

        pass

    @abstractmethod
    def step(self, formula: Any, traces, timestamps) -> float:
        """Evaluate the robustness of a subformula of a requirement.

        Arguments:
            traces: Set of trajectories.
            timestamps: List of corresponding timestamps.

        Returns:
            The robustness metric of the subformula over the selected
            window of the trace.
        """
        pass

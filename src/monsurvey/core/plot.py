from typing import Dict, List
import random

import plotly.graph_objs as go

from .result import Result
from .trajectory import Trajectory

class Plotter:
    """Plot monitor results.
    """

    _results: List[Result]

    def __init__(self, results: List[Result], trajectories: List[Trajectory]=None) -> None:
        self._results = results
        self._trajectories = trajectories

    def trajectories(self, show=True, save=False, filename: str=None, title: str="") -> None:
        """Plot the state value at each timestamp
        """

        subfigs = []

        if self._trajectories:
            for trajectory in self._trajectories:
                for state, trace in trajectory.states.items():
                    subfigs.append(
                        go.Scatter(
                            name=trajectory.label + ": " + state,
                            x=trajectory.timestamps,
                            y=trace,
                            mode="lines",
                            showlegend=True
                    )
                )

            fig = go.Figure(subfigs)
            fig.update_layout(
                title=title,
                xaxis_title="Timestamp",
                yaxis_title="Value"
            )

            fig.write_image(filename) if save and filename else None
            fig.show() if show else None

    def robustness_per_timestamp(self, show=True, save=False, filename: str=None, title: str="") -> None:
        """Plot the robustness at each timestamp for each Result.
        """

        scale = 0
        subfigs = []

        for result in self._results:
            for subformula, evaluations in result.evaluations.items():
                scale += 1
                subfigs.append(
                    go.Scatter(
                        name=result.label + ": " + subformula,
                        x=[z.timestamp for z in evaluations],
                        y=[z.robustness for z in evaluations],
                        mode="lines",
                        line=dict(dash=f"{scale}px {scale}px {scale}px {scale}px"),
                        showlegend=True
                    )
                )

        fig = go.Figure(subfigs)
        fig.update_layout(
            title=title,
            xaxis_title="Timestamp",
            yaxis_title="Robustness"
        )

        fig.write_image(filename) if save and filename else None
        fig.show() if show else None

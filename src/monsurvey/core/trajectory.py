from dataclasses import dataclass
from typing import List, Dict

@dataclass
class Trajectory:
    """A state and timestamp trajectory.
    """

    label: str
    timestamps: List[float]
    states: Dict[str, List[float]]

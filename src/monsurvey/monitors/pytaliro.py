import time
from typing import Dict, List

import numpy as np
import numpy.typing as npt

from taliro import tptaliro as tp

from ..core.specification import Specification
from ..core.requirement import Requirement, Subformula
from ..core.result import Evaluation, Result

class TpTaliroSpecification(Specification):
    """Py-TaLiRo's TP-TaLiRo monitor specification.
    """

    _requirement: Requirement
    _auxiliary: List[Dict[str, np.ndarray]]

    def __init__(self, requirement: Requirement, auxiliary: List[Dict[str, np.ndarray]]) -> None:
        self._requirement = requirement
        self._auxiliary = []

        self._auxiliary = [{
            "name": x["name"],
            "a": np.array(x["a"], dtype=np.double, ndmin=2),
            "b": np.array(x["b"], dtype=np.double, ndmin=2)
        } for x in auxiliary]

    def evaluate(self, traces: npt.ArrayLike, timestamps: npt.ArrayLike, iterations=None) -> Result:
        """Evaluate the robustness of a TP-TaLiRo requirement.
        """

        traces = np.array(traces, dtype=np.double, ndmin=2).T
        timestamps = np.array(timestamps, dtype=np.double, ndmin=2).T

        assert(traces.shape[0] == timestamps.shape[0])

        iterations = timestamps.shape[0] if not iterations else iterations

        result = Result(label="TP-TaLiRo (Py-TaLiRo)")
        for i in range(iterations):
            for subformula in self._requirement:
                result.append(
                    key=subformula.label,
                    robustness=self.step(
                        formula=subformula,
                        traces=traces[i:],
                        timestamps=timestamps[i:]
                    )
                )

        return result

    def step(self, formula: Subformula, traces: npt.ArrayLike, timestamps: npt.ArrayLike) -> Evaluation:
        """Evaluate the robustness of a TP-TaLiRo subformula.
        """

        delta = timestamps.tolist()[0][0]

        states = traces.T
        times = np.array([[round(float(x - delta), 3) for x in times] for times in timestamps.tolist()], dtype=np.double, ndmin=2).T

        start = time.time()
        robustness = tp.tptaliro(
            spec=formula.phi,
            preds=self._auxiliary,
            st=states,
            ts=times
        )
        end = time.time()

        return Evaluation(
            elapsed=abs(end - start),
            robustness=robustness["ds"],
            timestamp=round(float(timestamps.tolist()[0][0]), 3)
        )

import time
from typing import Dict

import numpy as np
import numpy.typing as npt

from rtamt import (
    Language,
    STLDiscreteTimeSpecification,
    STLDenseTimeSpecification,
    Semantics
)

from ..core.specification import Specification
from ..core.requirement import Requirement, Subformula
from ..core.result import Evaluation, Result

class DiscreteSpecification(Specification):
    """RTAMT (Discrete) monitor specification.
    """

    _requirement: Requirement
    _auxiliary: Dict[str, int]
    _step: float

    def __init__(self, requirement: Requirement, auxiliary: Dict[str, int]) -> None:
        self._requirement = requirement
        self._auxiliary = auxiliary

    def evaluate(self, traces: npt.ArrayLike, timestamps: npt.ArrayLike, iterations=None) -> Result:
        """Evaluate the robustness of an RTAMT requirement.
        """

        traces = np.array(traces, dtype=np.float64, ndmin=2)
        timestamps = np.array(timestamps, dtype=np.float64, ndmin=1)

        # # quick fix for RTAMT (Discrete) sampling conflict
        # traces = np.array(
        #     np.concatenate(
        #         (traces, np.array(traces[-1], ndmin=1)),
        #         axis=0
        #     ), dtype=np.float64, ndmin=2
        # )

        # # quick fix for RTAMT (Discrete) sampling conflict
        # timestamps = np.array(
        #     np.concatenate(
        #         (timestamps, np.array(timestamps[-1], ndmin=1)),
        #         axis=0
        #     ), dtype=np.float64, ndmin=1
        # )

        assert(traces.shape[1] == timestamps.shape[0])

        # set Discrete sampling period
        self._step = round(timestamps[1] - timestamps[0], 3)

        # format data
        trajectories = {}
        for name, column in self._auxiliary.items():
            trajectories[name] = traces[column].tolist()

        iterations = (timestamps.shape[0] - 1) if not iterations else iterations

        result = Result(label="RTAMT (Discrete)")
        for i in range(iterations):
            for subformula in self._requirement:
                result.append(
                    key=subformula.label,
                    robustness=self.step(
                        formula=subformula,
                        traces={name: trace[i:] for name, trace in trajectories.items()},
                        timestamps=timestamps[i:].tolist()
                    )
                )

        return result

    def step(self, formula: Subformula, traces: npt.ArrayLike, timestamps: npt.ArrayLike) -> Evaluation:
        """Evaluate the robustness of an RTAMT (Discrete) subformula.
        """

        # setup RTAMT (Discrete) specification
        obj = STLDiscreteTimeSpecification(
            Semantics.STANDARD,
            language=Language.PYTHON
        )

        for name in self._auxiliary.keys():
            obj.declare_var(name, "float")

        # set discrete monitor sampling period property
        obj.set_sampling_period(self._step, "s", 0.1)

        obj.spec = formula.phi
        obj.parse()

        # make timestamps relative
        times = [round(x - timestamps[0], 3) for x in timestamps]

        trajectories = dict(traces, **{"time": times})

        start = time.time()
        robustnesses = obj.evaluate(trajectories)
        end = time.time()

        return Evaluation(
            elapsed=abs(end - start),
            robustness=robustnesses[0][1],
            timestamp=timestamps[0]
        )

class DenseSpecification(Specification):
    """RTAMT (Dense) monitor specification.
    """

    _requirement: Requirement
    _auxiliary: Dict[str, int]
    _obj: STLDenseTimeSpecification

    def __init__(self, requirement: Requirement, auxiliary: Dict[str, int]) -> None:
        self._requirement = requirement
        self._auxiliary = auxiliary

    def evaluate(self, traces: npt.ArrayLike, timestamps: npt.ArrayLike, iterations=None) -> Result:
        """Evaluate the robustness of an RTAMT requirement.
        """

        traces = np.array(traces, dtype=np.float64, ndmin=2)
        timestamps = np.array(timestamps, dtype=np.float64, ndmin=1)

        assert(traces.shape[1] == timestamps.shape[0])

        # format data
        trajectories = {}
        for name, column in self._auxiliary.items():
            trajectories[name] = traces[column].tolist()

        iterations = timestamps.shape[0] if not iterations else iterations

        result = Result(label="RTAMT (Dense)")
        for i in range(iterations):
            for subformula in self._requirement:
                result.append(
                    key=subformula.label,
                    robustness=self.step(
                        formula=subformula,
                        traces={name: trace[i:] for name, trace in trajectories.items()},
                        timestamps=timestamps[i:].tolist()
                    )
                )

        return result

    def step(self, formula: Subformula, traces: npt.ArrayLike, timestamps: npt.ArrayLike) -> Evaluation:
        """Evaluate the robustness of an RTAMT (Dense) subformula.
        """

        # setup RTAMT (Dense) specification
        obj = STLDenseTimeSpecification(
            Semantics.STANDARD,
            language=Language.PYTHON
        )

        for name in self._auxiliary.keys():
            obj.declare_var(name, "float")

        obj.spec = formula.phi
        obj.parse()
        #obj.pastify()

        # modify timestamps to be relative
        times = np.array([round(x - timestamps[0], 3) for x in timestamps])

        trajectories = [
            (name, np.array([times, trace]).T.tolist()) for name, trace in traces.items()
        ]

        start = time.time()
        robustness = obj.evaluate(*trajectories)
        end = time.time()

        return Evaluation(
            elapsed=abs(end - start),
            robustness=robustness[0][1] if robustness else np.inf,
            timestamp=timestamps[0]
        )

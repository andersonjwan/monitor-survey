from os import path
import time
from typing import Dict

try:
    import matlab
    import matlab.engine
except ImportError:
    _has_matlab = False
else:
    _has_matlab = True

import numpy as np
import numpy.typing as npt

from ..core.specification import Specification
from ..core.requirement import Requirement, Subformula
from ..core.result import Evaluation, Result

class DpTaliroSpecification(Specification):
    """DP-TaLiRo (MATLAB) monitor specification.
    """

    def __init__(self, dptaliro: str, requirement: Requirement, auxiliary, eng=None) -> None:
        """
        Arguments:
            dptaliro: Path to DP-TaLiRo (MATLAB) script (i.e., `dp_taliro.m`)
            requirement: A valid requirement for DP-TaLiRo (MATLAB).
            auxiliary: Predicate map.
        """

        if not _has_matlab:
            raise RuntimeError(
                ("DP-TaLiRo (MATLAB) requires MATLAB and the MATLAB Engine API for Python to be installed. "
                 "Please refer to the README to enable this monitor.")
            )

        self._eng = eng if eng else matlab.engine.start_matlab()
        self._eng.addpath(dptaliro)
        self._eng.addpath(path.join(path.dirname(path.realpath(__file__)), "matlab"))

        self._requirement = requirement
        self._auxiliary = auxiliary

    def evaluate(self, traces: npt.ArrayLike, timestamps: npt.ArrayLike, iterations=None) -> Result:
        """Evaluate the robustess of a DP-TaLiRo (MATLAB) requirment.
        """

        traces = np.array(traces, dtype=float, ndmin=2).T
        timestamps = np.array(timestamps, dtype=float, ndmin=2).T

        assert(traces.shape[0] == timestamps.shape[0])

        iterations = timestamps.shape[0] if not iterations else iterations

        result = Result(label="DP-TaLiRo (MATLAB)")
        for i in range(iterations):
            for subformula in self._requirement:
                result.append(
                    key=subformula.label,
                    robustness=self.step(
                        formula=subformula,
                        traces=traces[i:],
                        timestamps=timestamps[i:]
                    )
                )

        return result

    def step(self, formula: Subformula, traces: npt.ArrayLike, timestamps: npt.ArrayLike) -> Evaluation:
        """Evaluate the robustness of a DP-TaLiRo (MATLAB) subformula.
        """

        delta = timestamps.tolist()[0][0]

        states = matlab.double(traces.tolist())
        times = matlab.double([[x - delta for x in times] for times in timestamps.tolist()])

        start = time.time()
        robustness = self._eng.dptaliro(
            formula.phi,
            self._auxiliary,
            states,
            times
        )
        end = time.time()

        return Evaluation(
            elapsed=abs(end - start),
            robustness=robustness,
            timestamp=timestamps.tolist()[0][0]
        )

class TpTaliroSpecification(Specification):
    """TP-TaLiRo (MATLAB) monitor specification.
    """

    def __init__(self, tptaliro: str, requirement: Requirement, auxiliary, eng=None) -> None:
        """
        Arguments:
            tptaliro: Path to TP-TaLiRo (MATLAB) script (i.e., `tp_taliro.m`)
            requirement: A valid requirement for TP-TaLiRo.
            auxiliary: Predicate map.
        """

        if not _has_matlab:
            raise RuntimeError(
                ("DP-TaLiRo (MATLAB) requires MATLAB and the MATLAB Engine API for Python to be installed. "
                 "Please refer to the README to enable this monitor.")
            )

        self._eng = eng if eng else matlab.engine.start_matlab()
        self._eng.addpath(tptaliro)
        self._eng.addpath(path.join(path.dirname(path.realpath(__file__)), "matlab"))

        self._requirement = requirement
        self._auxiliary = auxiliary

    def evaluate(self, traces: npt.ArrayLike, timestamps: npt.ArrayLike, iterations=None) -> Result:
        """Evaluate the robustess of a TP-TaLiRo (MATLAB) requirment.
        """

        traces = np.array(traces, dtype=float, ndmin=2).T
        timestamps = np.array(timestamps, dtype=float, ndmin=2).T

        assert(traces.shape[0] == timestamps.shape[0])

        iterations = timestamps.shape[0] if not iterations else iterations

        result = Result(label="TP-TaLiRo (MATLAB)")
        for i in range(iterations):
            for subformula in self._requirement:
                result.append(
                    key=subformula.label,
                    robustness=self.step(
                        formula=subformula,
                        traces=traces[i:],
                        timestamps=timestamps[i:]
                    )
                )

        return result

    def step(self, formula: Subformula, traces: npt.ArrayLike, timestamps: npt.ArrayLike) -> Evaluation:
        """Evaluate the robustness of a TP-TaLiRo (MATLAB) subformula.
        """

        delta = timestamps.tolist()[0][0]

        states = matlab.double(traces.tolist())
        times = matlab.double([[x - delta for x in times] for times in timestamps.tolist()])

        start = time.time()
        robustness = self._eng.tptaliro(
            formula.phi,
            self._auxiliary,
            states,
            times
        )
        end = time.time()

        return Evaluation(
            elapsed=abs(end - start),
            robustness=robustness,
            timestamp=timestamps.tolist()[0][0]
        )

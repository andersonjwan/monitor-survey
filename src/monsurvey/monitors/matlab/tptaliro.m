function rob = tptaliro(formula, predicates, traces, timestamps)
    pmap = struct;
    for i = 1:length(predicates)
        pred = predicates(i);
        pred = pred{1, 1};

        A = pred.A;
        if iscell(A)
            A = cell2mat(A);
        end

        b = pred.b;
        if iscell(b)
            b = cell2mat(b);
        end

        % copy predicate to predicate map
        pmap(i).str = convertStringsToChars(pred.str);
        pmap(i).A = double(A);
        pmap(i).b = double(b);
    end

    % run TP-TaLiRo
    rob = tp_taliro(formula, pmap, traces, timestamps);
end
import time
from typing import Dict

import numpy as np
import numpy.typing as npt

from ..core.specification import Specification
from ..core.requirement import Requirement, Subformula
from ..core.result import Evaluation, Result

class TLTkSpecification(Specification):
    """TLTk monitor specification.
    """

    _requirement: Requirement
    _auxiliary: Dict[str, int]

    def __init__(self, requirement: Requirement, auxiliary: Dict[str, int]) -> None:
        self._requirement = requirement
        self._auxiliary = auxiliary

    def evaluate(self, traces: npt.ArrayLike, timestamps: npt.ArrayLike, iterations=None) -> Result:
        """Evaluate the robustness of a TLTk requirement.
        """

        np_traces = np.array(traces, dtype=np.float64, ndmin=2)
        np_times = np.array(timestamps, dtype=np.float32)

        # ensure number of samples are equivalent
        assert(np_traces.shape[1] == np_times.shape[0])

        # format data
        trajectories = {name: np_traces[index] for name, index in self._auxiliary.items()}

        iterations = np_times.shape[0] if not iterations else iterations

        result = Result(label="TLTk")
        for i in range(iterations):
            for subformula in self._requirement:
                result.append(
                    key=subformula.label,
                    robustness=self.step(
                        formula=subformula,
                        traces={name: trace[i:] for name, trace in trajectories.items()},
                        timestamps=np_times[i:]
                    )
                )

        return result

    def step(self, formula: Subformula, traces: npt.ArrayLike, timestamps: npt.ArrayLike) -> Evaluation:
        """Evaluate the robustness of a TLTk sub-formula.
        """

        formula.phi.reset()

        times = np.array([round(x - timestamps[0], 3) for x in timestamps], dtype=np.float32)

        start = time.time()
        formula.phi.eval_interval(
            traces,
            times,
        )
        end = time.time()

        return Evaluation(
            elapsed=abs(end - start),
            robustness=formula.phi.robustness,
            timestamp=timestamps[0]
        )

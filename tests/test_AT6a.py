from os import path
from unittest import TestCase, skip, skipIf

try:
    import matlab
except ImportError:
    _has_matlab = False
else:
    _has_matlab = True

import pandas as pd

from monsurvey.core.plot import Plotter
from monsurvey.core.requirement import Requirement, Subformula

from monsurvey.monitors.tltk import TLTkSpecification
from monsurvey.monitors.rtamt import DiscreteSpecification, DenseSpecification
from monsurvey.monitors import pytaliro
from monsurvey.monitors import staliro

class AT6aTestCase(TestCase):
    @classmethod
    def setUpClass(self) -> None:
        self._mwengine = matlab.engine.start_matlab() if _has_matlab else None

        self._data = {}
        testdir = path.dirname(path.realpath(__file__))

        data = pd.read_csv(path.join(testdir, "data", "autotrans", "AT6a_trace1_small.csv"))
        self._data["trace1"] = {
            "traces": [list(data["v"]), list(data["w"])],
            "timestamps": list(data["timestamps"])
        }

        data = pd.read_csv(path.join(testdir, "data", "autotrans", "AT6a_trace2_small.csv"))
        self._data["trace2"] = {
            "traces": [list(data["v"]), list(data["w"])],
            "timestamps": list(data["timestamps"])
        }

        self._results = {"trace1": [], "trace2": []}

    @classmethod
    def tearDownClass(self) -> None:
        for trace, result in self._results.items():
            plot = Plotter(result)
            plot.robustness_per_timestamp(
                save=True,
                filename=f"AT6a_{trace}_results.svg",
                title=f"Robustness per Timestamp (AT6a, Trace {trace[-1]})"
            )

    def test_tltk(self) -> None:
        import tltk_mtl as mtl

        for trace, trajectories in self._data.items():
            specification = TLTkSpecification(
                Requirement([
                    Subformula(
                        "phi", mtl.Or(
                            mtl.Not(
                                mtl.Global(
                                    0, 1.2, mtl.Predicate("w", 1, 3000))),
                            mtl.Global(
                                0, 0.25, mtl.Predicate("v", 1, 35))
                        )
                    )
                ]),
                {"v": 0, "w": 1}
            )

            self._results[trace].append(specification.evaluate(
                trajectories["traces"],
                trajectories["timestamps"]
            ))

    def test_rtamt_discrete(self) -> None:
        for trace, trajectories in self._data.items():
            specification = DiscreteSpecification(
                Requirement([
                    Subformula("phi", "(always[0, 1.2](w <= 3000)) implies (always[0, 0.25](v <= 35))")
                ]),
                {"v": 0, "w": 1}
            )

            self._results[trace].append(specification.evaluate(
                trajectories["traces"],
                trajectories["timestamps"]
            ))

    def test_rtamt_dense(self) -> None:
        for trace, trajectories in self._data.items():
            specification = DenseSpecification(
                Requirement([
                    Subformula("phi", "(always[0, 1.2](w <= 3000)) implies (always[0, 0.25](v <= 35))")
                ]),
                {"v": 0, "w": 1}
            )

            self._results[trace].append(specification.evaluate(
                trajectories["traces"],
                trajectories["timestamps"]
            ))

    @skip("TP-TaLiRo (Py-TaLiRo) currently disabled due to bug with multi-dimension trajectories")
    def test_pytaliro_tptaliro(self) -> None:
        for trace, trajectories in self._data.items():
            specification = pytaliro.TpTaliroSpecification(
                Requirement([
                    Subformula("phi", "(@Var_t1 [](({ Var_t1 >= 0 } /\ { Var_t1 <= 1.2 }) -> p1)) -> (@Var_t2 [](({ Var_t2 >= 0 } /\ { Var_t2 <= 0.25 }) -> p2))")
                ]),
                [{"name": "p1", "a": 1.0, "b": 3000}, {"name": "p2", "a": 1.0, "b": 35}]
            )

            self._results[trace].append(specification.evaluate(
                trajectories["traces"],
                trajectories["timestamps"]
            ))

    @skipIf(not _has_matlab, "MATLAB Engine API for Python must be installed to compare against S-TaLiRo")
    def test_staliro_dptaliro(self) -> None:
        for trace, trajectories in self._data.items():
            specification = staliro.DpTaliroSpecification(
                path.join(path.expanduser("~"), "dp_taliro"),
                Requirement([
                    Subformula("phi", "([]_[0, 1.2] (w)) -> ([]_[0, 0.25] (v))")
                ]),
                [{"str": "w", "A": [0, 1], "b": 3000}, {"str": "v", "A": [1, 0], "b": 35}],
                self._mwengine
            )

            self._results[trace].append(specification.evaluate(
                trajectories["traces"],
                trajectories["timestamps"]
            ))

    @skipIf(not _has_matlab, "MATLAB Engine API for Python must be installed to compare against S-TaLiRo")
    def test_staliro_tptaliro(self) -> None:
        for trace, trajectories in self._data.items():
            specification = staliro.TpTaliroSpecification(
                path.join(path.expanduser("~"), "tp_taliro"),
                Requirement([
                    Subformula("phi", "(@Var_t1 [](({ Var_t1 >= 0 } /\ { Var_t1 <= 1.2 }) -> w)) -> (@Var_t2 [](({ Var_t2 >= 0 } /\ { Var_t2 <= 0.25 }) -> v))")
                ]),
                [{"str": "w", "A": [0, 1], "b": 3000}, {"str": "v", "A": [1, 0], "b": 35}],
                self._mwengine
            )

            self._results[trace].append(specification.evaluate(
                trajectories["traces"],
                trajectories["timestamps"]
            ))

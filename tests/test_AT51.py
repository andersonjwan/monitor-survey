from os import path
from unittest import TestCase, skip, skipIf

try:
    import matlab
except ImportError:
    _has_matlab = False
else:
    _has_matlab = True

import pandas as pd

from monsurvey.core.plot import Plotter
from monsurvey.core.requirement import Requirement, Subformula
from monsurvey.core.trajectory import Trajectory

from monsurvey.monitors.tltk import TLTkSpecification
from monsurvey.monitors.rtamt import DiscreteSpecification, DenseSpecification
from monsurvey.monitors import pytaliro
from monsurvey.monitors import staliro

class AT51TestCase(TestCase):
    @classmethod
    def setUpClass(self) -> None:
        self._mwengine = matlab.engine.start_matlab() if _has_matlab else None

        self._data = {}
        self._trajectories = []

        testdir = path.dirname(path.realpath(__file__))

        data = pd.read_csv(path.join(testdir, "data", "autotrans", "AT51_trace1.csv"))
        self._data["trace1"] = {
            "traces": list(data["gear"]),
            "timestamps": list(data["t"])
        }

        self._trajectories.append(
            Trajectory(
                label="Trace 1",
                timestamps=list(data["t"]),
                states={"gear": list(data["gear"])}
            )
        )

        data = pd.read_csv(path.join(testdir, "data", "autotrans", "AT51_trace2.csv"))
        self._data["trace2"] = {
            "traces": list(data["gear"]),
            "timestamps": list(data["t"])
        }

        self._trajectories.append(
            Trajectory(
                label="Trace 2",
                timestamps=list(data["t"]),
                states={"gear": list(data["gear"])}
            )
        )

        data = pd.read_csv(path.join(testdir, "data", "autotrans", "AT51_trace3.csv"))
        self._data["trace3"] = {
            "traces": list(data["gear"]),
            "timestamps": list(data["t"])
        }

        self._trajectories.append(
            Trajectory(
                label="Trace 3",
                timestamps=list(data["t"]),
                states={"gear": list(data["gear"])}
            )
        )

        data = pd.read_csv(path.join(testdir, "data", "autotrans", "AT51_trace4.csv"))
        self._data["trace4"] = {
            "traces": list(data["gear"]),
            "timestamps": list(data["t"])
        }

        self._trajectories.append(
            Trajectory(
                label="Trace 4",
                timestamps=list(data["t"]),
                states={"gear": list(data["gear"])}
            )
        )

        self._results = {"trace1": [], "trace2": [], "trace3": [], "trace4": []}

    @classmethod
    def tearDownClass(self) -> None:
        for trace, result in self._results.items():
            plot = Plotter(result, self._trajectories)

            plot.robustness_per_timestamp(
                save=True,
                filename=f"AT51_{trace}_results.svg",
                title=f"Robustness per Timestamp (AT51, Trace {trace[-1]})"
            )

        plot.trajectories(
            save=True,
            filename="AT51_Traces.svg",
            title="Active Gear per Timestamp"
        )

    def test_tltk(self) -> None:
        import tltk_mtl as mtl

        for trace, trajectories in self._data.items():
            specification = TLTkSpecification(
                Requirement([
                    Subformula(
                        "phi", mtl.Global(
                            0, 30, mtl.Or(
                                mtl.Not(
                                    mtl.And(
                                        mtl.Not(
                                            mtl.And(
                                                mtl.Predicate("p1", -1, -0.5),
                                                mtl.Predicate("p2", 1, 1.5)
                                            )
                                        ),
                                        mtl.Next(
                                            mtl.And(
                                                mtl.Predicate("p3", -1, -0.5),
                                                mtl.Predicate("p4", 1, 1.5)
                                            )
                                        )
                                    )
                                ),
                                mtl.Next(
                                    mtl.Global(
                                        0, 2.5, mtl.And(
                                            mtl.Predicate("p5", -1, -0.5),
                                            mtl.Predicate("p6", 1, 1.5)
                                        )
                                    )
                                )
                            )
                        )
                    )
                ]),
                {"p1": 0, "p2": 0, "p3": 0, "p4": 0, "p5": 0, "p6": 0}
            )

            self._results[trace].append(specification.evaluate(
                trajectories["traces"],
                trajectories["timestamps"]
            ))

    def test_rtamt_discrete(self) -> None:
        for trace, trajectories in self._data.items():
            specification = DiscreteSpecification(
                Requirement([
                    Subformula("phi", "always[0, 30] ((!(g1 >= 0.5 and g1 <= 1.5) and next (g1 >= 0.5 and g1 <= 1.5)) implies (next always[0, 2.5] (g1 >= 0.5 and g1 <= 1.5)))")
                ]),
                {"g1": 0}
            )

            self._results[trace].append(specification.evaluate(
                trajectories["traces"],
                trajectories["timestamps"]
            ))

    def test_rtamt_dense(self) -> None:
        for trace, trajectories in self._data.items():
            specification = DenseSpecification(
                Requirement([
                    Subformula("phi", "always[0, 30] ((!(g1 >= 0.5 and g1 <= 1.5) and eventually[0.001, 0.1] (g1 >= 0.5 and g1 <= 1.5)) implies (eventually[0.001, 0.1] always[0, 2.5] (g1 >= 0.5 and g1 <= 1.5)))")
                ]),
                {"g1": 0}
            )

            self._results[trace].append(specification.evaluate(
                trajectories["traces"],
                trajectories["timestamps"]
            ))

from os import path
from unittest import TestCase, skipIf

try:
    import matlab
except ImportError:
    _has_matlab = False
else:
    _has_matlab = True

import pandas as pd

from monsurvey.core.plot import Plotter
from monsurvey.core.requirement import Requirement, Subformula

from monsurvey.monitors.tltk import TLTkSpecification
from monsurvey.monitors.rtamt import DiscreteSpecification, DenseSpecification
from monsurvey.monitors import pytaliro
from monsurvey.monitors import staliro

class ReqTestCase01(TestCase):
    @classmethod
    def setUpClass(self) -> None:
        self._mwengine = matlab.engine.start_matlab() if _has_matlab else None

        testdir = path.dirname(path.realpath(__file__))
        data = pd.read_csv(path.join(testdir, "data", "trajectory.csv"))

        self._traces = list(data["x1"])
        self._timestamps = list(data["t"])

        self._results = []

    @classmethod
    def tearDownClass(self) -> None:
        plot = Plotter(self._results)

        plot.robustness_per_timestamp(
            save=True,
            filename=f"req01_results.svg",
            title=f"Robustness per Timestamp (Req01)"
        )

    def test_tltk(self) -> None:
        import tltk_mtl as mtl
        specification = TLTkSpecification(
            Requirement([
                Subformula(
                    "phi", mtl.Not(
                        mtl.And(
                            mtl.Global(0, 4, mtl.And(mtl.Predicate("x1", 1, 250), mtl.Predicate("x1", -1, -240))),
                            mtl.Finally(3.5, 4.0, mtl.And(mtl.Predicate("x1", 1, 240.1), mtl.Predicate("x1", -1, -240.0)))
                        )
                    )
                )
            ]),
            {"x1": 0}
        )

        self._results.append(specification.evaluate(
            self._traces,
            self._timestamps
        ))

    def test_rtamt_discrete(self) -> None:
            specification = DiscreteSpecification(
                Requirement([
                    Subformula("phi", "(not ((always[0.0, 4.0]((x1 <= 250.0) and (x1 >= 240.0))) and (eventually[3.5,4.0]((x1 <= 240.1) and (x1 >= 240.0)))))")
                ]),
                {"x1": 0}
            )

            self._results.append(specification.evaluate(
                self._traces,
                self._timestamps
            ))

    def test_rtamt_dense(self) -> None:
            specification = DenseSpecification(
                Requirement([
                    Subformula("phi", "(not ((always[0.0, 4.0]((x1 <= 250.0) and (x1 >= 240.0))) and (eventually[3.5,4.0]((x1 <= 240.1) and (x1 >= 240.0)))))")
                ]),
                {"x1": 0}
            )

            self._results.append(specification.evaluate(
                self._traces,
                self._timestamps
            ))

    def test_pytaliro_tptaliro(self) -> None:
        specification = pytaliro.TpTaliroSpecification(
            Requirement([
                Subformula("phi", "!(@Var_t1 ([](({ Var_t1 >= 0 } /\ { Var_t1 <= 4.0 }) -> x1_1 /\ x1_2)) /\ (@Var_t2 <>((({ Var_t2 >= 3.5 } /\ { Var_t2 <= 4.0 }) /\ (x1_3 /\ x1_4)))))")
            ]),
            [{"name": "x1_1", "a": 1.0, "b": 250.0},
             {"name": "x1_2", "a": -1.0, "b": -240.0},
             {"name": "x1_3", "a": 1.0, "b": 240.1},
             {"name": "x1_4", "a": -1.0, "b": -240.0}]
        )

        self._results.append(specification.evaluate(
            self._traces,
            self._timestamps
        ))

    @skipIf(not _has_matlab, "MATLAB Engine API for Python must be installed to compare against S-TaLiRo")
    def test_staliro_dptaliro(self) -> None:
        specification = staliro.DpTaliroSpecification(
            path.join(path.expanduser("~"), "dp_taliro"),
            Requirement([
                Subformula("phi", "!(([]_[0, 4] (x1_1 /\ x1_2)) /\ (<>_[3.5, 4.0](x1_3 /\ x1_4)))")
            ]),
            [{"str": "x1_1", "A": 1.0, "b": 250.0},
             {"str": "x1_2", "A": -1.0, "b": -240.0},
             {"str": "x1_3", "A": 1.0, "b": 240.1},
             {"str": "x1_4", "A": -1.0, "b": -240.0}],
            self._mwengine
        )

        self._results.append(specification.evaluate(
            self._traces,
            self._timestamps
        ))

    @skipIf(not _has_matlab, "MATLAB Engine API for Python must be installed to compare against S-TaLiRo")
    def test_staliro_tptaliro(self) -> None:
        specification = staliro.TpTaliroSpecification(
            path.join(path.expanduser("~"), "tp_taliro"),
            Requirement([
                Subformula("phi", "!(@Var_t1 ([](({ Var_t1 >= 0 } /\ { Var_t1 <= 4.0 }) -> x1_1 /\ x1_2)) /\ (@Var_t2 <>((({ Var_t2 >= 3.5 } /\ { Var_t2 <= 4.0 }) /\ (x1_3 /\ x1_4)))))")
            ]),
            [{"str": "x1_1", "A": 1.0, "b": 250.0},
             {"str": "x1_2", "A": -1.0, "b": -240.0},
             {"str": "x1_3", "A": 1.0, "b": 240.1},
             {"str": "x1_4", "A": -1.0, "b": -240.0}],
            self._mwengine
        )

        self._results.append(specification.evaluate(
            self._traces,
            self._timestamps
        ))

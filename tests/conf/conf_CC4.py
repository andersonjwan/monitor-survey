from os import path
from unittest import TestCase, skipIf

try:
    import matlab
except ImportError:
    _has_matlab = False
else:
    _has_matlab = True

import pandas as pd

from monsurvey.core.plot import Plotter
from monsurvey.core.requirement import Requirement, Subformula

from monsurvey.monitors.tltk import TLTkSpecification
from monsurvey.monitors.rtamt import DiscreteSpecification, DenseSpecification
from monsurvey.monitors import pytaliro
from monsurvey.monitors import staliro

SIG_FIGS = 3

class CC4ConformanceTestCase(TestCase):
    @classmethod
    def setUpClass(self) -> None:
        self._mwengine = matlab.engine.start_matlab() if _has_matlab else None

        self._data = {}
        testdir = path.dirname(path.realpath(__file__))

        data = pd.read_csv(path.join(testdir, "..", "data", "chasingcars", "CC4_trace1_small.csv"))
        self._data["trace1"] = {
            "traces": list(data["y54"]),
            "timestamps": list(data["timestamps"])
        }

        data = pd.read_csv(path.join(testdir, "..", "data", "chasingcars", "CC4_trace2_small.csv"))
        self._data["trace2"] = {
            "traces": list(data["y54"]),
            "timestamps": list(data["timestamps"])
        }

        self._results = {"trace1": [], "trace2": []}

    def test_tltk(self) -> None:
        import tltk_mtl as mtl

        specification = TLTkSpecification(
            Requirement([
                Subformula(
                    "phi", mtl.Global(
                        0, 24, mtl.Finally(
                            0, 10, mtl.Global(
                                0, 0.25, mtl.Predicate(
                                    "y54", -1, -8
                                )
                            )
                        )
                    )
                )
            ]),
            {"y54": 0}
        )

        self.assertAlmostEqual(
            2.0,
            specification.evaluate(
                self._data["trace1"]["traces"],
                self._data["trace1"]["timestamps"],
                iterations=1
            ).evaluations["phi"][0].robustness,
            SIG_FIGS
        )

        self.assertAlmostEqual(
            2.0,
            specification.evaluate(
                self._data["trace2"]["traces"],
                self._data["trace2"]["timestamps"],
                iterations=1
            ).evaluations["phi"][0].robustness,
            SIG_FIGS
        )

    def test_rtamt_dense(self) -> None:
        specification = DenseSpecification(
            Requirement([
                Subformula("phi", "always[0, 24] eventually[0, 10] always[0, 0.25] (y54 >= 8)")
            ]),
            {"y54": 0}
        )

        self.assertAlmostEqual(
            2.0,
            specification.evaluate(
                self._data["trace1"]["traces"],
                self._data["trace1"]["timestamps"],
                iterations=1
            ).evaluations["phi"][0].robustness,
            SIG_FIGS
        )

        self.assertAlmostEqual(
            2.0,
            specification.evaluate(
                self._data["trace2"]["traces"],
                self._data["trace2"]["timestamps"],
                iterations=1
            ).evaluations["phi"][0].robustness,
            SIG_FIGS
        )

    def test_pytaliro_tptaliro(self) -> None:
        specification = pytaliro.TpTaliroSpecification(
            Requirement([
                Subformula("phi", "@Var_t1 [](({ Var_t1 >= 0 } /\ { Var_t1 <= 24 }) -> (@Var_t2 <>(({ Var_t2 >= 0 } /\ { Var_t2 <= 10 }) /\ (@Var_t3 [](({ Var_t3 >= 0 } /\ { Var_t3 <= 0.25 }) -> (p1))))))")
            ]),
            [{"name": "p1", "a": -1.0, "b": -8}]
        )

        self.assertAlmostEqual(
            2.0,
            specification.evaluate(
                self._data["trace1"]["traces"],
                self._data["trace1"]["timestamps"],
                iterations=1
            ).evaluations["phi"][0].robustness,
            SIG_FIGS
        )

        self.assertAlmostEqual(
            2.0,
            specification.evaluate(
                self._data["trace2"]["traces"],
                self._data["trace2"]["timestamps"],
                iterations=1
            ).evaluations["phi"][0].robustness,
            SIG_FIGS
        )

    @skipIf(not _has_matlab, "MATLAB Engine API for Python must be installed to compare against S-TaLiRo")
    def test_staliro_dptaliro(self) -> None:
        specification = staliro.DpTaliroSpecification(
            path.join(path.expanduser("~"), "dp_taliro"),
            Requirement([
                Subformula("phi", "[]_[0, 24] (<>_[0, 10] ([]_[0, 0.25] (p1)))")
            ]),
            [{"str": "p1", "A": -1.0, "b": -8}],
            self._mwengine
        )

        self.assertAlmostEqual(
            2.0,
            specification.evaluate(
                self._data["trace1"]["traces"],
                self._data["trace1"]["timestamps"],
                iterations=1
            ).evaluations["phi"][0].robustness,
            SIG_FIGS
        )

        self.assertAlmostEqual(
            2.0,
            specification.evaluate(
                self._data["trace2"]["traces"],
                self._data["trace2"]["timestamps"],
                iterations=1
            ).evaluations["phi"][0].robustness,
            SIG_FIGS
        )

    @skipIf(not _has_matlab, "MATLAB Engine API for Python must be installed to compare against S-TaLiRo")
    def test_staliro_tptaliro(self) -> None:
        specification = staliro.TpTaliroSpecification(
            path.join(path.expanduser("~"), "tp_taliro"),
            Requirement([
                Subformula("phi", "@Var_t1 [](({ Var_t1 >= 0 } /\ { Var_t1 <= 24 }) -> (@Var_t2 <>(({ Var_t2 >= 0 } /\ { Var_t2 <= 10 }) /\ (@Var_t3 [](({ Var_t3 >= 0 } /\ { Var_t3 <= 0.25 }) -> (p1))))))")
            ]),
            [{"str": "p1", "A": -1.0, "b": -8}],
            self._mwengine
        )

        self.assertAlmostEqual(
            2.0,
            specification.evaluate(
                self._data["trace1"]["traces"],
                self._data["trace1"]["timestamps"],
                iterations=1
            ).evaluations["phi"][0].robustness,
            SIG_FIGS
        )

        self.assertAlmostEqual(
            2.0,
            specification.evaluate(
                self._data["trace2"]["traces"],
                self._data["trace2"]["timestamps"],
                iterations=1
            ).evaluations["phi"][0].robustness,
            SIG_FIGS
        )

from os import path
from unittest import TestCase, skip, skipIf

try:
    import matlab
except ImportError:
    _has_matlab = False
else:
    _has_matlab = True

import pandas as pd

from monsurvey.core.plot import Plotter
from monsurvey.core.requirement import Requirement, Subformula

from monsurvey.monitors.tltk import TLTkSpecification
from monsurvey.monitors.rtamt import DiscreteSpecification, DenseSpecification
from monsurvey.monitors import pytaliro
from monsurvey.monitors import staliro

SIG_FIGS = 3

class AT6bConformanceTestCase(TestCase):
    @classmethod
    def setUpClass(self) -> None:
        self._mwengine = matlab.engine.start_matlab() if _has_matlab else None

        self._data = {}
        testdir = path.dirname(path.realpath(__file__))

        data = pd.read_csv(path.join(testdir, "..", "data", "autotrans", "AT6b_trace1_small.csv"))
        self._data["trace1"] = {
            "traces": [list(data["v"]), list(data["w"])],
            "timestamps": list(data["timestamps"])
        }

        data = pd.read_csv(path.join(testdir, "..", "data", "autotrans", "AT6b_trace2_small.csv"))
        self._data["trace2"] = {
            "traces": [list(data["v"]), list(data["w"])],
            "timestamps": list(data["timestamps"])
        }

        self._results = {"trace1": [], "trace2": []}

    def test_tltk(self) -> None:
        import tltk_mtl as mtl

        for trace, trajectories in self._data.items():
            specification = TLTkSpecification(
                Requirement([
                    Subformula(
                        "phi", mtl.Or(
                            mtl.Not(
                                mtl.Global(
                                    0, 1.2, mtl.Predicate("w", 1, 3000))),
                            mtl.Global(
                                0, 0.5, mtl.Predicate("v", 1, 50))
                        )
                    )
                ]),
                {"v": 0, "w": 1}
            )

        self.assertAlmostEqual(
            42.20770449649761,
            specification.evaluate(
                self._data["trace1"]["traces"],
                self._data["trace1"]["timestamps"],
                iterations=1
            ).evaluations["phi"][0].robustness,
            SIG_FIGS
        )

        self.assertAlmostEqual(
            40.64012814286189,
            specification.evaluate(
                self._data["trace2"]["traces"],
                self._data["trace2"]["timestamps"],
                iterations=1
            ).evaluations["phi"][0].robustness,
            SIG_FIGS
        )

    def test_rtamt_discrete(self) -> None:
        specification = DiscreteSpecification(
            Requirement([
                Subformula("phi", "(always[0, 1.2](w <= 3000)) implies (always[0, 0.5](v <= 50))")
            ]),
            {"v": 0, "w": 1}
        )

        self.assertAlmostEqual(
            42.20770449649761,
            specification.evaluate(
                self._data["trace1"]["traces"],
                self._data["trace1"]["timestamps"],
                iterations=1
            ).evaluations["phi"][0].robustness,
            SIG_FIGS
        )

        self.assertAlmostEqual(
            40.64012814286189,
            specification.evaluate(
                self._data["trace2"]["traces"],
                self._data["trace2"]["timestamps"],
                iterations=1
            ).evaluations["phi"][0].robustness,
            SIG_FIGS
        )

    def test_rtamt_dense(self) -> None:
        specification = DenseSpecification(
            Requirement([
                Subformula("phi", "(always[0, 1.2](w <= 3000)) implies (always[0, 0.5](v <= 50))")
            ]),
            {"v": 0, "w": 1}
        )

        self.assertAlmostEqual(
            42.20770449649761,
            specification.evaluate(
                self._data["trace1"]["traces"],
                self._data["trace1"]["timestamps"],
                iterations=1
            ).evaluations["phi"][0].robustness,
            SIG_FIGS
        )

        self.assertAlmostEqual(
            40.64012814286189,
            specification.evaluate(
                self._data["trace2"]["traces"],
                self._data["trace2"]["timestamps"],
                iterations=1
            ).evaluations["phi"][0].robustness,
            SIG_FIGS
        )

    @skip("TP-TaLiRo (Py-TaLiRo) currently disabled due to bug with multi-dimension trajectories")
    def test_pytaliro_tptaliro(self) -> None:
        specification = pytaliro.TpTaliroSpecification(
            Requirement([
                Subformula("phi", "(@Var_t1 [](({ Var_t1 >= 0 } /\ { Var_t1 <= 1.2 }) -> p1)) -> (@Var_t2 [](({ Var_t2 >= 0 } /\ { Var_t2 <= 0.5 }) -> p2))")
            ]),
            [{"name": "p1", "a": 1.0, "b": 3000}, {"name": "p2", "a": 1.0, "b": 50}]
        )

        self.assertAlmostEqual(
            42.20770449649761,
            specification.evaluate(
                self._data["trace1"]["traces"],
                self._data["trace1"]["timestamps"],
                iterations=1
            ).evaluations["phi"][0].robustness,
            SIG_FIGS
        )

        self.assertAlmostEqual(
            40.64012814286189,
            specification.evaluate(
                self._data["trace2"]["traces"],
                self._data["trace2"]["timestamps"],
                iterations=1
            ).evaluations["phi"][0].robustness,
            SIG_FIGS
        )

    @skipIf(not _has_matlab, "MATLAB Engine API for Python must be installed to compare against S-TaLiRo")
    def test_staliro_dptaliro(self) -> None:
        specification = staliro.DpTaliroSpecification(
            path.join(path.expanduser("~"), "dp_taliro"),
            Requirement([
                Subformula("phi", "([]_[0, 1.2] (w)) -> ([]_[0, 0.5] (v))")
            ]),
            [{"str": "w", "A": [0, 1], "b": 3000}, {"str": "v", "A": [1, 0], "b": 50}],
            self._mwengine
        )

        self.assertAlmostEqual(
            42.20770449649761,
            specification.evaluate(
                self._data["trace1"]["traces"],
                self._data["trace1"]["timestamps"],
                iterations=1
            ).evaluations["phi"][0].robustness,
            SIG_FIGS
        )

        self.assertAlmostEqual(
            40.64012814286189,
            specification.evaluate(
                self._data["trace2"]["traces"],
                self._data["trace2"]["timestamps"],
                iterations=1
            ).evaluations["phi"][0].robustness,
            SIG_FIGS
        )

    @skipIf(not _has_matlab, "MATLAB Engine API for Python must be installed to compare against S-TaLiRo")
    def test_staliro_tptaliro(self) -> None:
        specification = staliro.TpTaliroSpecification(
            path.join(path.expanduser("~"), "tp_taliro"),
            Requirement([
                Subformula("phi", "(@Var_t1 [](({ Var_t1 >= 0 } /\ { Var_t1 <= 1.2 }) -> w)) -> (@Var_t2 [](({ Var_t2 >= 0 } /\ { Var_t2 <= 0.5 }) -> v))")
            ]),
            [{"str": "w", "A": [0, 1], "b": 3000}, {"str": "v", "A": [1, 0], "b": 50}],
            self._mwengine
        )

        self.assertAlmostEqual(
            42.20770449649761,
            specification.evaluate(
                self._data["trace1"]["traces"],
                self._data["trace1"]["timestamps"],
                iterations=1
            ).evaluations["phi"][0].robustness,
            SIG_FIGS
        )

        self.assertAlmostEqual(
            40.64012814286189,
            specification.evaluate(
                self._data["trace2"]["traces"],
                self._data["trace2"]["timestamps"],
                iterations=1
            ).evaluations["phi"][0].robustness,
            SIG_FIGS
        )

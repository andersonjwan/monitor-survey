from os import path
from unittest import TestCase, skip, skipIf

import pandas as pd
import statistics as stats

from monsurvey.core.requirement import Requirement, Subformula
from monsurvey.monitors.rtamt import DiscreteSpecification

K_ITERATIONS = 10

class PerfTestCaseDiscreteRTAMT(TestCase):
    @classmethod
    def setUpClass(self) -> None:
        testdir = path.dirname(path.dirname(path.realpath(__file__)))
        data = pd.read_csv(path.join(testdir, "data", "long_trajectory.csv"))

        self._traces = data["x"].tolist()
        self._timestamps = data["t"].tolist()

        self._results = {}

    @classmethod
    def tearDownClass(self) -> None:
        outdata = {"tool": [], "phi": [], "elapsed": [], "variance": [], "robustness": []}

        for phi, results in self._results.items():
            outdata["tool"].append("rtamt_discrete")
            outdata["phi"].append(phi)

            outdata["elapsed"].append(stats.mean([x.evaluations[phi][0].elapsed for x in results]))
            outdata["variance"].append(stats.variance([x.evaluations[phi][0].elapsed for x in results]))
            outdata["robustness"].append(stats.mean([x.evaluations[phi][0].robustness for x in results]))

        pd.DataFrame.from_dict(outdata).to_csv(
            "perf_rtamt_discrete.csv", index=False, header=True
        )

    def test_req01(self) -> None:
        for i in range(K_ITERATIONS):
            specification = DiscreteSpecification(
                Requirement([
                    Subformula("phi1", "always (x >= 5.0)")
                ]),
                {"x": 0}
            )

            if self._results.get("phi1") is None:
                self._results["phi1"] = []

            self._results["phi1"].append(
                specification.evaluate(
                    traces=self._traces,
                    timestamps=self._timestamps,
                    iterations=1
                )
            )

    def test_req02(self) -> None:
        for i in range(K_ITERATIONS):
            specification = DiscreteSpecification(
                Requirement([
                    Subformula("phi2", "always[0,10] (x <= 1.0)")
                ]),
                {"x": 0}
            )

            if self._results.get("phi2") is None:
                self._results["phi2"] = []

            self._results["phi2"].append(
                specification.evaluate(
                    traces=self._traces,
                    timestamps=self._timestamps,
                    iterations=1
                )
            )

    def test_req03(self) -> None:
        for i in range(K_ITERATIONS):
            specification = DiscreteSpecification(
                Requirement([
                    Subformula("phi3", "eventually[50,75] (x >= 1.0)")
                ]),
                {"x": 0}
            )

            if self._results.get("phi3") is None:
                self._results["phi3"] = []

            self._results["phi3"].append(
                specification.evaluate(
                    traces=self._traces,
                    timestamps=self._timestamps,
                    iterations=1
                )
            )

    def test_req04(self) -> None:
        for i in range(K_ITERATIONS):
            specification = DiscreteSpecification(
                Requirement([
                    Subformula("phi4", "always[1000,1100] ((x >= -1.0) and (x <= 2.0))")
                ]),
                {"x": 0}
            )

            if self._results.get("phi4") is None:
                self._results["phi4"] = []

            self._results["phi4"].append(
                specification.evaluate(
                    traces=self._traces,
                    timestamps=self._timestamps,
                    iterations=1
                )
            )

    def test_req05(self) -> None:
        for i in range(K_ITERATIONS):
            specification = DiscreteSpecification(
                Requirement([
                    Subformula("phi5", "always[10,25] ((x >= 1.0) implies (eventually[0,10] (x <= 0.0)))")
                ]),
                {"x": 0}
            )

            if self._results.get("phi5") is None:
                self._results["phi5"] = []

            self._results["phi5"].append(
                specification.evaluate(
                    traces=self._traces,
                    timestamps=self._timestamps,
                    iterations=1
                )
            )

    def test_req06(self) -> None:
        for i in range(K_ITERATIONS):
            specification = DiscreteSpecification(
                Requirement([
                    Subformula("phi6", "always[100,125] ((x >= 0.0) implies (always[0,20] (eventually[0,5] ((x <= 2.0) and (x >= 1.0)))))")
                ]),
                {"x": 0}
            )

            if self._results.get("phi6") is None:
                self._results["phi6"] = []

            self._results["phi6"].append(
                specification.evaluate(
                    traces=self._traces,
                    timestamps=self._timestamps,
                    iterations=1
                )
            )

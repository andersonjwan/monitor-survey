from os import path
from unittest import TestCase, skip, skipIf

import pandas as pd
import statistics as stats

import tltk_mtl as mtl

from monsurvey.core.requirement import Requirement, Subformula
from monsurvey.monitors.tltk import TLTkSpecification

K_ITERATIONS = 10

class PerfTestCaseTLTk(TestCase):
    @classmethod
    def setUpClass(self) -> None:
        testdir = path.dirname(path.dirname(path.realpath(__file__)))
        data = pd.read_csv(path.join(testdir, "data", "long_trajectory.csv"))

        self._traces = data["x"].tolist()
        self._timestamps = data["t"].tolist()

        self._results = {}

    @classmethod
    def tearDownClass(self) -> None:
        outdata = {"tool": [], "phi": [], "elapsed": [], "variance": [], "robustness": []}

        for phi, results in self._results.items():
            outdata["tool"].append("tltk")
            outdata["phi"].append(phi)

            outdata["elapsed"].append(stats.mean([x.evaluations[phi][0].elapsed for x in results]))
            outdata["variance"].append(stats.variance([x.evaluations[phi][0].elapsed for x in results]))
            outdata["robustness"].append(stats.mean([x.evaluations[phi][0].robustness for x in results]))

        pd.DataFrame.from_dict(outdata).to_csv(
            "perf_tltk.csv", index=False, header=True
        )

    def test_req01(self) -> None:
        for i in range(K_ITERATIONS):
            specification = TLTkSpecification(
                Requirement([
                    Subformula("phi1", mtl.Global(0, float('inf'), mtl.Predicate("p1", -1.0, -5.0)))
                ]),
                {"p1": 0}
            )

            if self._results.get("phi1") is None:
                self._results["phi1"] = []

            self._results["phi1"].append(
                specification.evaluate(
                    traces=self._traces,
                    timestamps=self._timestamps,
                    iterations=1
                )
            )

    def test_req02(self) -> None:
        for i in range(K_ITERATIONS):
            specification = TLTkSpecification(
                Requirement([
                    Subformula("phi2", mtl.Global(0, 10, mtl.Predicate("p1", 1.0, 1.0)))
                ]),
                {"p1": 0}
            )

            if self._results.get("phi2") is None:
                self._results["phi2"] = []

            self._results["phi2"].append(
                specification.evaluate(
                    traces=self._traces,
                    timestamps=self._timestamps,
                    iterations=1
                )
            )

    def test_req03(self) -> None:
        for i in range(K_ITERATIONS):
            specification = TLTkSpecification(
                Requirement([
                    Subformula("phi3", mtl.Finally(50, 75, mtl.Predicate("p1", -1.0, -1.0)))
                ]),
                {"p1": 0}
            )

            if self._results.get("phi3") is None:
                self._results["phi3"] = []

            self._results["phi3"].append(
                specification.evaluate(
                    traces=self._traces,
                    timestamps=self._timestamps,
                    iterations=1
                )
            )

    def test_req04(self) -> None:
        for i in range(K_ITERATIONS):
            specification = TLTkSpecification(
                Requirement([
                    Subformula("phi4", mtl.Global(1000, 1100, mtl.And(mtl.Predicate("p1", -1.0, 1.0), mtl.Predicate("p2", 1.0, 2.0))))
                ]),
                {"p1": 0, "p2": 0}
            )

            if self._results.get("phi4") is None:
                self._results["phi4"] = []

            self._results["phi4"].append(
                specification.evaluate(
                    traces=self._traces,
                    timestamps=self._timestamps,
                    iterations=1
                )
            )

    def test_req05(self) -> None:
        for i in range(K_ITERATIONS):
            specification = TLTkSpecification(
                Requirement([
                    Subformula("phi5", mtl.Global(10, 25, mtl.Or(mtl.Not(mtl.Predicate("p1", -1.0, -1.0)), mtl.Finally(0, 10, mtl.Predicate("p2", 1.0, 0.0)))))
                ]),
                {"p1": 0, "p2": 0}
            )

            if self._results.get("phi5") is None:
                self._results["phi5"] = []

            self._results["phi5"].append(
                specification.evaluate(
                    traces=self._traces,
                    timestamps=self._timestamps,
                    iterations=1
                )
            )

    def test_req06(self) -> None:
        for i in range(K_ITERATIONS):
            specification = TLTkSpecification(
                Requirement([
                    Subformula(
                        "phi6", mtl.Global(
                            100, 125, mtl.Or(
                                mtl.Not(mtl.Predicate("p1", -1.0, 0.0)),
                                mtl.Global(0, 20, mtl.Finally(0, 5, mtl.And(
                                    mtl.Predicate("p2", 1.0, 2.0),
                                    mtl.Predicate("p3", -1.0, -1.0)
                                )))
                            )
                        )
                    )
                ]),
                {"p1": 0, "p2": 0, "p3": 0}
            )

            if self._results.get("phi6") is None:
                self._results["phi6"] = []

            self._results["phi6"].append(
                specification.evaluate(
                    traces=self._traces,
                    timestamps=self._timestamps,
                    iterations=1
                )
            )

from os import path
from unittest import TestCase, skip, skipIf

import pandas as pd
import statistics as stats

from monsurvey.core.requirement import Requirement, Subformula
from monsurvey.monitors.pytaliro import TpTaliroSpecification

K_ITERATIONS = 10

class PerfTestCaseTpTaliroPyTaliro(TestCase):
    @classmethod
    def setUpClass(self) -> None:

        testdir = path.dirname(path.dirname(path.realpath(__file__)))
        data = pd.read_csv(path.join(testdir, "data", "long_trajectory.csv"))

        self._traces = data["x"].tolist()
        self._timestamps = data["t"].tolist()

        self._results = {}

    @classmethod
    def tearDownClass(self) -> None:
        outdata = {"tool": [], "phi": [], "elapsed": [], "variance": [], "robustness": []}

        for phi, results in self._results.items():
            outdata["tool"].append("pytaliro_tptaliro")
            outdata["phi"].append(phi)

            outdata["elapsed"].append(stats.mean([x.evaluations[phi][0].elapsed for x in results]))
            outdata["variance"].append(stats.variance([x.evaluations[phi][0].elapsed for x in results]))
            outdata["robustness"].append(stats.mean([x.evaluations[phi][0].robustness for x in results]))

        pd.DataFrame.from_dict(outdata).to_csv(
            "perf_pytaliro_tptaliro.csv", index=False, header=True
        )

    def test_req01(self) -> None:
        for i in range(K_ITERATIONS):
            specification = TpTaliroSpecification(
                Requirement([
                    Subformula("phi1", "[] (p1)")
                ]),
                [{"name": "p1", "a": -1.0, "b": -5.0}]
            )

            if self._results.get("phi1") is None:
                self._results["phi1"] = []

            self._results["phi1"].append(
                specification.evaluate(
                    traces=self._traces,
                    timestamps=self._timestamps,
                    iterations=1
                )
            )

    def test_req02(self) -> None:
        for i in range(K_ITERATIONS):
            specification = TpTaliroSpecification(
                Requirement([
                    Subformula("phi2", "@Var_t [](({ Var_t >= 0 } /\ { Var_t <= 10 }) -> (p1))")
                ]),
                [{"name": "p1", "a": 1.0, "b": 1.0}]
            )

            if self._results.get("phi2") is None:
                self._results["phi2"] = []

            self._results["phi2"].append(
                specification.evaluate(
                    traces=self._traces,
                    timestamps=self._timestamps,
                    iterations=1
                )
            )

    def test_req03(self) -> None:
        for i in range(K_ITERATIONS):
            specification = TpTaliroSpecification(
                Requirement([
                    Subformula("phi3", "@Var_t <>(({ Var_t >= 50 } /\ { Var_t <= 75 }) /\ (p1))")
                ]),
                [{"name": "p1", "a": -1.0, "b": -1.0}]
            )

            if self._results.get("phi3") is None:
                self._results["phi3"] = []

            self._results["phi3"].append(
                specification.evaluate(
                    traces=self._traces,
                    timestamps=self._timestamps,
                    iterations=1
                )
            )

    def test_req04(self) -> None:
        for i in range(K_ITERATIONS):
            specification = TpTaliroSpecification(
                Requirement([
                    Subformula("phi4", "@Var_t [](({ Var_t >= 1000 } /\ { Var_t <= 1100 }) -> ((p1) /\ (p2)))")
                ]),
                [{"name": "p1", "a": -1.0, "b": 1.0}, {"name": "p2", "a": 1.0, "b": 2.0}]
            )

            if self._results.get("phi4") is None:
                self._results["phi4"] = []

            self._results["phi4"].append(
                specification.evaluate(
                    traces=self._traces,
                    timestamps=self._timestamps,
                    iterations=1
                )
            )

    def test_req05(self) -> None:
        for i in range(K_ITERATIONS):
            specification = TpTaliroSpecification(
                Requirement([
                    Subformula("phi5", "@Var_t1 [](({ Var_t1 >= 10 } /\ { Var_t1 <= 25 }) -> ((p1) -> (@Var_t2 <>(({ Var_t2 >= 0 } /\ { Var_t2 <= 10 }) /\ (p2)))))")
                ]),
                [{"name": "p1", "a": -1.0, "b": -1.0}, {"name": "p2", "a": 1.0, "b": 0.0}]
            )

            if self._results.get("phi5") is None:
                self._results["phi5"] = []

            self._results["phi5"].append(
                specification.evaluate(
                    traces=self._traces,
                    timestamps=self._timestamps,
                    iterations=1
                )
            )

    def test_req06(self) -> None:
        for i in range(K_ITERATIONS):
            specification = TpTaliroSpecification(
                Requirement([
                    Subformula("phi6", "@Var_t1 [](({ Var_t1 >= 100 } /\ { Var_t1 <= 125 }) -> ((p1) -> (@Var_t2 [](({ Var_t2 >= 0 } /\ { Var_t2 <= 20 }) -> (@Var_t3 <>(({ Var_t3 >= 0 } /\ { Var_t3 <= 5 }) /\ ((p2) /\ (p3))))))))")
                ]),
                [{"name": "p1", "a": -1.0, "b": 0.0}, {"name": "p2", "a": 1.0, "b": 2.0}, {"name": "p3", "a": -1.0, "b": -1.0}]
            )

            if self._results.get("phi6") is None:
                self._results["phi6"] = []

            self._results["phi6"].append(
                specification.evaluate(
                    traces=self._traces,
                    timestamps=self._timestamps,
                    iterations=1
                )
            )

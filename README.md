# Monitor Survey

The monitor survey tool attempts to provide a common interface to compare various offline monitors used in the verification and testing of Cyber-Physical Systems. The current set of tools supported include:

- [Py-TaLiRo](https://pypi.org/project/py-taliro/): TP-TaLiRo
- [RTAMT](https://github.com/nickovic/rtamt): Discrete-Time, Dense-Time
- [S-TaLiRo](https://app.assembla.com/spaces/s-taliro_public/subversion/source): DP-TaLiRo, TP-TaLiRo
- [TLTk](https://bitbucket.org/versyslab/tltk/src/master/)

## Getting Started

*A Unix-based environment is assumed. For more support, please contact <andersonjwan@gmail.com> or open an [issue](https://gitlab.com/andersonjwan/monitor-survey/-/issues).*

The main dependency from this repository is [Poetry](https://python-poetry.org/). It is used to manage all Python-related dependencies. Therefore, this should be installed to run the survey.

With Poetry installed, run the following command to install the dependencies and relevant packages:

```bash
poetry install
```

To ensure consistency, all dependencies are installed from the `poetry.lock` file. Therefore, unless the lockfile is modified, everyone who clones the repository will conduct the comparison with the same monitor versions.

## Run Monitor Survey

To run the survey, you must invoke the Poetry environment and run the set of `unittests`. In short, run the following command:

```bash
poetry run python -m unittest discover --verbose --start "tests/" --pattern "test_*.py"
```

**Note**: By default, the MATLAB-based tools are disabled. To enable, the following should be complete:

1. Install [MATLAB](https://www.mathworks.com/products/matlab.html) and add to PATH if not already present
2. Install the [MATLAB Engine API for Python](https://www.mathworks.com/help/matlab/matlab-engine-for-python.html). Installation instructions found [here](https://www.mathworks.com/help/matlab/matlab_external/install-the-matlab-engine-for-python.html)

For Step 2, since we are using Poetry, you must first activate the virtual environment before installing the Python support engine from MATLAB. This can be done with the following command from the project's root directory:

```bash
poetry shell
```

You can now run the MATLAB Engine API for Python installation command which usually resolves to the following command:

```bash
cd "matlabroot/extern/engines/python"
python setup.py install
```

## Results

The test results plot the robustness of each tool from the requirement. This is stored in an `svg` format with the following naming convention `{testname}_{trace}_results.svg`.

The current set of requirements include AT6a, AT6b, and CC4 from

>Ernst, Gidon, Paolo Arcaini, Ismail Bennani, Aniruddh Chandratre, Alexandre Donzé, Georgios Fainekos, Goran Frehse et al. "ARCH-COMP 2021 Category Report: Falsification with Validation of Results." EPiC Series in Computing 80 (2021): 133-152.

with an additional simple requirement.

